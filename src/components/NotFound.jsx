import React from 'react';
import { Link } from 'react-router';

export default function NotFound() {
  return (
    <div>
      <h1>Not Found</h1>
      <button><Link style={{ textDecoration: 'none', color: 'black', cursor: 'default' }} to="/">Home</Link></button>
    </div>
  );
}
