import React from 'react';
import * as firebase from 'firebase';

export default class Boxes extends React.Component {
  constructor() {
    super();

    this.state = {
      boxes: [],
      loaded: false
    };
  }

  componentDidMount() {
    this.database = firebase.database();
    this.userBoxesRef = this.database.ref(`users/${window.loggedInUserId}/boxes`);

    this.userBoxesRef.on('value', (userBoxes) => {
      this.boxRefs = [];
      if (userBoxes.val()) {
        Object.keys(userBoxes.val()).forEach((boxId) => {
          const boxRef = this.database.ref(`boxes/${boxId}`);
          boxRef.on('value', (box) => {
            this.setState({
              boxes: this.state.boxes.concat(box.val()),
              loaded: true
            });
          });
          this.boxRefs.push(boxRef);
        });
      } else {
        this.setState({
          loaded: true
        });
      }
    });
  }

  componentWillUnmount() {
    this.userBoxesRef.off('value');
    this.boxRefs.forEach((boxRef) => boxRef.off('value'));
  }

  render() {
    let el;
    if (this.state.loaded) {
      if (this.state.boxes.length === 0) {
        el = (<div>You have no boxes</div>);
      } else {
        el = (<div>{this.state.boxes.map((box, index) => (<div key={index}><h4>{box.name}</h4><p>{box.description}</p></div>))}</div>);
      }
    } else {
      el = (<div>Loading...</div>);
    }

    return (
      <div>
        <h1>Boxes</h1>
        {el}
      </div>
    );
  }
}
