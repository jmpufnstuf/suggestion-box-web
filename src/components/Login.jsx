import React from 'react';
import * as firebase from 'firebase';
import { Link } from 'react-router';

export default class Login extends React.Component {
  constructor() {
    super();
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);

    this.state = {
      email: '',
      password: '',
      loading: false,
      loginErrorMessage: '',
      loggedIn: false,
      loggedInUser: null
    };
  }

  handleEmailChange(e) {
    e.preventDefault();

    this.setState({
      email: e.target.value,
      loginErrorMessage: ''
    });
  }

  handlePasswordChange(e) {
    e.preventDefault();

    this.setState({
      password: e.target.value,
      loginErrorMessage: ''
    });
  }

  handleLogin(e) {
    e.preventDefault();
    this.setState({
      loading: true
    });

    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {
      this.setState({
        loading: false,
        loggedIn: true,
        loggedInUser: firebase.auth().currentUser.email
      });

      window.loggedInUserId = firebase.auth().currentUser.uid;
    }).catch((error) => {
      // const errorCode = error.code;
      const errorMessage = error.message;

      this.setState({
        loading: false,
        loginErrorMessage: errorMessage
      });
    });
  }

  handleLogout(e) {
    e.preventDefault();

    if (!this.state.loggedIn) {
      return null;
    } else {
      firebase.auth().signOut().then(() => {
        this.setState({
          loggedIn: false,
          logoutErrorMessage: ''
        });
      }, (error) => {
        this.setState({
          logoutErrorMessage: error.message
        });
      });
    }
  }

  render() {
    return (
      <div>
        { this.state.loggedIn ?
          <div>
            <div>Welcome {this.state.loggedInUser}</div>
            <button><Link style={{ textDecoration: 'none', color: 'black', cursor: 'default' }} to="boxes">Boxes</Link></button>
            <button onClick={this.handleLogout}>Log out</button>{ this.state.loading ? <div className="fa fa-spinner fa-spin" /> : null }
            <div>{this.state.logoutErrorMessage}</div>
          </div> :
          <div>
            <form onSubmit={this.handleLogin}>
              <input type="email" placeholder="Email address" value={this.state.email} onChange={this.handleEmailChange}></input>
              <input type="password" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}></input>
              <button type="submit">Login</button>
              { this.state.loading ? <div className="fa fa-spinner fa-spin" /> : null }
            </form>
            <div>{this.state.loginErrorMessage}</div>
            <button><Link style={{ textDecoration: 'none', color: 'black', cursor: 'default' }} to="signup">Sign Up</Link></button>
          </div>
        }
      </div>
    );
  }
}
