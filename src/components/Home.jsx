import React from 'react';
import { Link } from 'react-router';

export default function Home() {
  return (
    <div>
      <h1>Home Page</h1>
      <button><Link style={{ textDecoration: 'none', color: 'black', cursor: 'default' }} to="login">Log In</Link></button>
      <button><Link style={{ textDecoration: 'none', color: 'black', cursor: 'default' }} to="signup">Sign Up</Link></button>
    </div>
  );
}
