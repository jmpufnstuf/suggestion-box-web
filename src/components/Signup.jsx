import React from 'react';
import * as firebase from 'firebase';

export default class Signup extends React.Component {
  constructor() {
    super();
    this.handleSignup = this.handleSignup.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);

    this.state = {
      email: '',
      password: '',
      loading: false,
      signupErrorMessage: '',
      signedUp: false,
      signedUpUser: null
    };
  }

  handleEmailChange(e) {
    e.preventDefault();

    this.setState({
      email: e.target.value,
      signupErrorMessage: ''
    });
  }

  handlePasswordChange(e) {
    e.preventDefault();

    this.setState({
      password: e.target.value,
      signupErrorMessage: ''
    });
  }

  handleSignup(e) {
    e.preventDefault();
    this.setState({
      loading: true
    });

    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then(() => {
      this.setState({
        loading: false,
        signedUp: true,
        signedUpUser: firebase.auth().currentUser.email
      });
    }).catch((error) => {
      // const errorCode = error.code;
      const errorMessage = error.message;

      this.setState({
        loading: false,
        signupErrorMessage: errorMessage
      });
    });
  }

  render() {
    return (
      <div>
        { this.state.signedUp ?
          <div>
            <div>Welcome {this.state.signedUpUser}</div>
          </div> :
          <div>
            <form onSubmit={this.handleSignup}>
              <input type="email" placeholder="Email address" value={this.state.email} onChange={this.handleEmailChange}></input>
              <input type="password" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange}></input>
              <button type="submit">Sign Up</button>
              { this.state.loading ? <div className="fa fa-spinner fa-spin" /> : null }
            </form>
            <div>{this.state.signupErrorMessage}</div>
          </div>
        }
      </div>
    );
  }
}
