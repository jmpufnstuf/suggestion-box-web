import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as firebase from "firebase";
import './index.css';

const uuidV4 = require('uuid/v4');

console.log(uuidV4());

const firebaseConfig = {
  apiKey: "AIzaSyAqH5LT_xMfMYpi1lwEnK4xYE6tZ0dXw8Q",
  authDomain: "suggestion-box-8b372.firebaseapp.com",
  databaseURL: "https://suggestion-box-8b372.firebaseio.com",
  storageBucket: "suggestion-box-8b372.appspot.com",
  messagingSenderId: "568261582443"
};
firebase.initializeApp(firebaseConfig);

const rootEl = document.getElementById('root');

ReactDOM.render(
  <App />,
  rootEl
);

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default;
    ReactDOM.render(
      <NextApp />,
      rootEl
    );
  });
}
