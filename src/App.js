import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import Home from './components/Home';
import Login from './components/Login';
import NotFound from './components/NotFound';
import Signup from './components/Signup';
import Boxes from './components/Boxes';

class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/">
          <IndexRoute component={Home} />
          <Route path="login" component={Login} />
          <Route path="signup" component={Signup} />
          <Route path="boxes" component={Boxes} />
          {/* <Route path="users" component={Users}>
            <Route path="/user/:userId" component={User} />
          </Route> */}
          <Route path="*" component={NotFound} />
        </Route>
      </Router>
    );
  }
}

export default App;
